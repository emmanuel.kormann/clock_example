import pyaudio

P = pyaudio.PyAudio()
CHUNK = 1024
FORMAT = pyaudio.paFloat32
CHANNELS = 2
RATE = 44100
