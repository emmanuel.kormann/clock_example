from threading import Thread
from audio import P, FORMAT, CHANNELS, RATE, CHUNK
import numpy

TEMPO = 60
CLOCK_CHANNEL = 1


def decode(in_data, channels):
    """
    Convert a byte stream into a 2D numpy array with
    shape (chunk_size, channels)

    Samples are interleaved, so for a stereo stream with left channel
    of [L0, L1, L2, ...] and right channel of [R0, R1, R2, ...], the output
    is ordered as [L0, R0, L1, R1, ...]
    """
    # TODO: handle data type as parameter, convert between pyaudio/numpy types
    result = numpy.fromstring(in_data, dtype=np.float32)

    chunk_length = len(result) / channels
    assert chunk_length == int(chunk_length)

    result = numpy.reshape(result, (chunk_length, channels))
    return result


def encode(signal):
    """
    Convert a 2D numpy array into a byte stream for PyAudio

    Signal should be a numpy array with shape (chunk_size, channels)
    """
    interleaved = signal.flatten()

    # TODO: handle data type as parameter, convert between pyaudio/numpy types
    out_data = interleaved.astype(numpy.float32).tobytes()
    return out_data


class Clock(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.is_running = True

    def stop(self):
        self.is_running = False

    def build_sample(self):
        length = 0.1
        frequency = 900.0
        volume = 1
        bip_timeline = numpy.linspace(0, length, RATE * length)
        trim_timeline = numpy.arange((RATE * 60 / TEMPO) - len(bip_timeline))
        timeline = numpy.concatenate((bip_timeline, trim_timeline))
        bip = numpy.sin(frequency * 2 * numpy.pi * timeline) * volume
        silence = bip * 0
        channels = numpy.tile(silence, (CHANNELS,1))
        channels[CLOCK_CHANNEL] = bip
        self.sample = numpy.swapaxes(channels, 0, 1)

    def rise_tempo(self):
        global TEMPO
        TEMPO = TEMPO + 10
        self.build_sample()

    def reduce_tempo(self):
        global TEMPO
        TEMPO = TEMPO - 10
        self.build_sample()


    def run(self):

        stream = P.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    output=True,
                    frames_per_buffer=CHUNK)

        self.build_sample()

        while self.is_running:
            stream.write(encode(self.sample))

        stream.stop_stream()
        stream.close()


clock = Clock()
