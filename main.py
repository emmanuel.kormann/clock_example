import sys, termios, tty
from multiprocessing import Process, Pipe

from server import run_server
from clock import clock
from event import getCharacter

def main():
    proc = Process(target=run_server, args=(), daemon=True)
    proc.start()
    clock.start()

    while True:
        character = getCharacter()
        if character == 'q':
            proc.terminate()
            clock.stop()
            return


main()
