from fastapi import FastAPI
from starlette.responses import HTMLResponse
from starlette.websockets import WebSocket
from starlette.templating import Jinja2Templates
from starlette.staticfiles import StaticFiles
import uvicorn

from clock import clock

app = FastAPI()
templates = Jinja2Templates(directory='ui/templates')
app.mount('/static', StaticFiles(directory='ui/static'), name='static')


@app.route('/')
async def homepage(request):
    return templates.TemplateResponse('index.html', {'request': request})


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()

    while True:
        data = await websocket.receive_text()
        if data == '+':
            clock.rise_tempo()
        from clock import TEMPO
        await websocket.send_text(f"Message text was: {TEMPO}")


def run_server():
    uvicorn.run(app, host='127.0.0.1', port=8000)
